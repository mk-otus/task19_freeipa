# task19_freeipa

LDAP
1. Установить FreeIPA
2. Написать playbook для конфигурации клиента
3. Настроить авторизацию по ssh-ключам (доп.задание)

--------------------------------------------------------------------
Запустим дефолтные машинки через Vagrantfile


192.168.100.10  ipa.otus.lan
192.168.100.11  client1.otus.lan

```
Предположим что у нас установлен уже freeipa server
Для добавления клиентов в домен, буду пользоваться готовым решением, а не пилить велосипеды
git clone https://github.com/freeipa/ansible-freeipa.git
cd ansible-freeipa

меняем фаил инвентори под свои переменные
vim 
[ipaclients]
client1.otus.lan                ansible_host=192.168.100.11

#[ipaservers]
#ipa.mine.dom

[ipaclients:vars]
ipaclient_domain=otus.lan
ipaclient_realm=OTUS.LAN
ipaadmin_principal=admin
ipaadmin_password=Password1
ipaclient_allow_repair=yes

#ipaclient_keytab=/tmp/krb5.keytab
#ipaclient_use_otp=yes
#ipaclient_force_join=yes
#ipaclient_kinit_attempts=3
#ipaclient_mkhomedir=yes
#ipaclient_use_otp=yes
```



и запустим плейбук c сервера, будет ходить пользователем vagrant
ansible-playbook -v -i inventory/hosts install-client.yml -b





